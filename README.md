# Signal Monitoring and HWC Notebooks utility packages

## Installation

This package is available on [acc-py](https://acc-py-repo.cern.ch/browse/). To install this package locally, please install first [acc-py-pip-config](https://gitlab.cern.ch/acc-co/devops/python/acc-py-pip-config), and proceed with the following command:

```console
pip install lhcsmnb
```

## Deployment

This package is automatically deployed to acc-py on tag.

> Note: For SWAN deployment, please see [sigmon-swan-deployer repository](https://gitlab.cern.ch/mpe-ms/sigmon/sigmon-swan-deployer/).
