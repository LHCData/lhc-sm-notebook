from assertpy import assert_that, fail
import pandas as pd
import pytest

from lhcsmnb import utils


@pytest.fixture(name="dummy_dataframe")
def fixture_dummy_dataframe():
    return pd.DataFrame({"column1": [1, 2, 3], "column2": [4, 5, 6]})


def test_append_to_column_name(dummy_dataframe):
    new_df = utils.append_to_column_name(dummy_dataframe, 0, "_appended")

    assert_that(new_df.columns[0]).is_equal_to("column1_appended")


def test_append_to_column_name_empty_df():
    new_df = utils.append_to_column_name(pd.DataFrame(), 0, "_appended")

    pd.testing.assert_frame_equal(new_df, pd.DataFrame())


@pytest.mark.parametrize("column_index", [2, -1])
def test_append_to_column_name_exception(dummy_dataframe, column_index):
    try:
        utils.append_to_column_name(dummy_dataframe, column_index, "_appended")
        fail("append_to_column_name should have raised error")
    except IndexError as err:
        assert_that(str(err)).is_equal_to("column_index is out of bounds")


def test_rename_column_name(dummy_dataframe):
    new_df = utils.rename_column_name(dummy_dataframe, 0, "new_name")

    assert_that(new_df.columns[0]).is_equal_to("new_name")


def test_rename_column_name_emty_df():
    new_df = utils.rename_column_name(pd.DataFrame(), 0, "new_name")

    pd.testing.assert_frame_equal(new_df, pd.DataFrame())


@pytest.mark.parametrize("column_index", [2, -1])
def test_rename_column_name_exception(dummy_dataframe, column_index):
    try:
        utils.rename_column_name(dummy_dataframe, column_index, "_appended")
        fail("rename_column_name should have raised error")
    except IndexError as err:
        assert_that(str(err)).is_equal_to("column_index is out of bounds")


def test_get_at(dummy_dataframe):
    result = utils.get_at(dummy_dataframe, 0, "column1")
    assert result == 1


def test_get_at_no_row(dummy_dataframe):
    expected = "expected"
    result = utils.get_at(dummy_dataframe, 1500, "column1", default="expected")
    assert result == expected


def test_get_at_no_column(dummy_dataframe):
    expected = "expected"
    result = utils.get_at(dummy_dataframe, 0, "NonExisting", default="expected")
    assert result == expected
