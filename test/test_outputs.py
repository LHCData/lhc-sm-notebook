import os
from io import StringIO
from pathlib import Path
from unittest import mock

import nbformat
import pytest

from lhcsmnb import outputs
from lhcsmnb.outputs import NotebookOutput, CellOutputType


def test_get_html_ids():
    html = (
        '<style type="text/css">\n'
        "</style>\n"
        '<table id="T_fd55d_">\n'
        "  <thead>\n"
        "    <tr>\n"
        '      <th class="blank level0" >&nbsp;</th>\n'
        '      <th class="col_heading level0 col3" >result</th>\n'
        "    </tr>\n"
        "  </thead>\n"
        "  <tbody>\n"
        "    <tr>\n"
        '      <th id="T_fd55d_level0_row0" class="row_heading level0 row0" >tau_i_meas</th>\n'
        '      <td id="T_fd55d_row0_col3" class="data row0 col3" >True</td>\n'
        "    </tr>\n"
        "    <tr>\n"
        '      <th id="T_fd55d_level0_row1" class="row_heading level0 row1" >tau_i_meas_ref</th>\n'
        '      <td id="T_fd55d_row1_col3" class="data row1 col3" >True</td>\n'
        "    </tr>\n"
        "  </tbody>\n"
        "</table>\n"
    )

    expected = ["T_fd55d_", "T_fd55d_level0_row0", "T_fd55d_level0_row1", "T_fd55d_row0_col3", "T_fd55d_row1_col3"]

    result = outputs._get_html_ids(html)
    assert expected == result


QUERY_LOGS = [
    (
        "\tQuerying PM event timestamps for system: FGC, className: 51_self_pmd, source: RPMBC.UL14.RTQX1.L1, "
        "RPHGC.UL14.RTQX2.L1, RPHFC.UL14.RQX.L1 from 2015-03-15 19:31:01.768 to 2015-03-15 19:59:56.517\n",
        "\tQuerying PM event timestamps for system: FGC, className: 51_self_pmd, source: RPHFC.UL14.RQX.L1, "
        "RPHGC.UL14.RTQX2.L1, RPMBC.UL14.RTQX1.L1 from 2015-03-15 19:31:01.768 to 2015-03-15 19:59:56.517\n",
        True,
    ),
    (
        "\tQuerying PM event signal(s) IEARTH.I_EARTH, IAB.I_A, STATUS.I_MEAS, STATUS.I_REF, STATUS.I_EARTH_PCNT"
        " for system: RPTE.UA23.RB.A12, className: lhc_self_pmd, source: FGC at 2021-05-01 11:04:37.220\n",
        "\tQuerying PM event signal(s) STATUS.I_MEAS, IEARTH.I_EARTH, IAB.I_A, STATUS.I_EARTH_PCNT, STATUS.I_REF"
        " for system: RPTE.UA23.RB.A12, className: lhc_self_pmd, source: FGC at 2021-05-01 11:04:37.220\n",
        True,
    ),
    (
        "\tQuerying PM event signal(s) STATUS.I_MEAS, STATUS.I_EARTH_PCNT, IEARTH.IEARTH for system: "
        "RPTE.UA23.RB.A12, className: 51_self_pmd, source: FGC at 2018-03-17 16:00:28.680\n",
        "\tQuerying PM event signal(s) STATUS.I_EARTH_PCNT, STATUS.I_MEAS, IEARTH.IEARTH for system: "
        "RPTE.UA23.RB.A12, className: 51_self_pmd, source: FGC at 2018-03-17 16:00:28.680\n",
        True,
    ),
    (
        "\tQuerying NXCALS signal(s) RB.A12.EVEN:ST_ABORT_PIC, RB.A12.ODD:ST_ABORT_PIC from "
        "2021-05-01 11:04:36.220 to 2021-05-01 11:05:37.220\n",
        "\tQuerying NXCALS signal(s) RB.A12.ODD:ST_ABORT_PIC, RB.A12.EVEN:ST_ABORT_PIC from "
        "2021-05-01 11:04:36.220 to 2021-05-01 11:05:37.220\n",
        True,
    ),
    (
        "\tQuerying NXCALS signal(s) MB.B11R1:U_EARTH_RB, MB.B8R1:U_EARTH_RB, MB.A10R1:U_EARTH_RB, "
        "MB.A8R1:U_EARTH_RB, MB.C12R1:U_EARTH_RB... from 2021-05-01 11:03:47.220 to 2021-05-01 11:10:27.220\n",
        "\tQuerying NXCALS signal(s) MB.C12R1:U_EARTH_RB, MB.B8R1:U_EARTH_RB, MB.A8R1:U_EARTH_RB, "
        "MB.A10R1:U_EARTH_RB, MB.B11R1:U_EARTH_RB... from 2021-05-01 11:03:47.220 to 2021-05-01 11:10:27.220\n",
        True,
    ),
    (
        "Querying Post Mortem failed using the following query: http://pm-rest.cern.ch/v2/pmdata/signal?system"
        "=QPS&className=DQAMCNMB_PMSTD&source=C12L2&timestampInNanos=1619859949901000000&signal=C12L2:U_QS0\n",
        "Querying Post Mortem failed using the following query: http://pm-rest.cern.ch/v2/pmdata/signal?system"
        "=QPS&className=DQAMCNMB_PMSTD&source=C12L2&timestampInNanos=1619859949901000000&signal=C12L2:U_QS0\n",
        False,
    ),
    (
        "\tQuerying NXCALS signal(s) MB.B11R1:U_EARTH_RB, MB.B8R1:U_EARTH_RB, MB.A10R1:U_EARTH_RB"
        ", MB.C12R1:U_EARTH_RB... from 2021-05-01 11:03:47.220 to 2021-05-01 11:10:27.220\n",
        "\tQuerying PM event signal(s) MB.C12R1:U_EARTH_RB, MB.B8R1:U_EARTH_RB, MB.A8R1:U_EARTH_RB"
        ", MB.B11R1:U_EARTH_RB... from 2021-05-01 11:03:47.220 to 2021-05-01 11:10:27.220\n",
        False,
    ),
]


@pytest.mark.parametrize("log,ref,expected", QUERY_LOGS)
def test_query_logs_equals(log, ref, expected):
    assert outputs._query_logs_equals(log, ref) == expected


ALLOWED_DIFF = [
    ("Analysis performed by root", "Analysis performed by lhcsm"),
    ("Analysis executed with lhc-sm-api version: 1.5.17", "Analysis executed with lhc-sm-api version: 1.5.16"),
    (
        "Analysis executed with lhc-sm-hwc notebooks version: 1.5.65",
        "Analysis executed with lhc-sm-hwc notebooks version: 1.5.64",
    ),
    ("Elapsed: 171.931 s.", "Elapsed: 230.758 s."),
    (
        "/tmp/lhcsmapi/analysis/dfb/DfbAnalysis.py:206: UserWarning:",
        "/tmp/lhcsmapi/analysis/dfb/DfbAnalysis.py:282: UserWarning:",
    ),
    (
        "[NbConvertApp] Writing 374364 bytes to ./results/reports/AN_60A_FPA.html",
        "[NbConvertApp] Writing 375004 bytes to ./results/reports/AN_60A_FPA.html",
    ),
    ("CPU times: user 6.07 s, sys: 74.9 ms, total: 6.14 s", "CPU times: user 6.11 s, sys: 64.7 ms, total: 6.18 s"),
    ("Wall time: 7.3 s", "Wall time: 7.35 s"),
    (
        "/tmp/ipykernel_74903/3127608717.py:3: UserWarning: WARNINIG: High Level!",
        "/tmp/ipykernel_69635/3127608717.py:3: UserWarning: WARNINIG: High Level!",
    ),
]


@pytest.mark.parametrize("log,ref", ALLOWED_DIFF)
def test_can_differ(log, ref):
    assert outputs._can_differ(log, ref)


NOTEBOOKS = [
    "result_AN_RB_PLI3.a5.ipynb",
    "result_AN_RQ_PLI3.a5.ipynb",
    "result_AN_RQ_PNO.a6.ipynb",
    "result_AN_RB_FPA.ipynb",
    "result_AN_IT_FPA.ipynb",
]


@pytest.mark.parametrize("notebook", NOTEBOOKS)
def test_notebook_outputs_are_equal(notebook):
    notebook_1 = _get_notebook("run1", notebook)
    notebook_2 = _get_notebook("run2", notebook)

    assert outputs.NotebookOutput(notebook_1) == outputs.NotebookOutput(notebook_2)


@pytest.mark.parametrize("notebook", NOTEBOOKS)
def test_notebook_outputs_are_not_equal(notebook):
    notebook_1 = _get_notebook("run1", notebook)
    notebook_2 = _get_notebook("run3", notebook)

    assert outputs.NotebookOutput(notebook_1) != outputs.NotebookOutput(notebook_2)


def test_compare_equal_notebook():
    dir1 = os.path.dirname(__file__) + "/run1"
    dir2 = os.path.dirname(__file__) + "/run2"

    with mock.patch("sys.stdout", new=StringIO()) as output:
        result = outputs.compare_notebooks(dir1, dir2, notebooks={"result_AN_RQ_PNO.a6.ipynb"})
        assert result == []
        assert output.getvalue() == "result_AN_RQ_PNO.a6.ipynb\n"


def test_compare_equal_notebooks():
    dir1 = os.path.dirname(__file__) + "/run1"
    dir2 = os.path.dirname(__file__) + "/run2"

    with mock.patch("sys.stdout", new=StringIO()) as output:
        result = outputs.compare_notebooks(dir1, dir2)
        assert result == []
        assert set(output.getvalue().split("\n")) == {
            "result_AN_RB_FPA.ipynb",
            "result_AN_RQ_PNO.a6.ipynb",
            "result_AN_RB_PLI3.a5.ipynb",
            "result_AN_RQ_PLI3.a5.ipynb",
            "result_AN_IT_FPA.ipynb",
            "",
        }


PARAMETERS = [
    ({outputs.CellOutputType.STDERR}, set()),
    ({outputs.CellOutputType.STDOUT}, {"result_AN_IT_FPA.ipynb"}),
    (
        {outputs.CellOutputType.IMAGE},
        {"result_AN_RQ_PNO.a6.ipynb", "result_AN_RB_PLI3.a5.ipynb", "result_AN_RQ_PLI3.a5.ipynb"},
    ),
    (
        {outputs.CellOutputType.HTML},
        {
            "result_AN_RB_PLI3.a5.ipynb",
            "result_AN_RQ_PLI3.a5.ipynb",
            "result_AN_RQ_PNO.a6.ipynb",
            "result_AN_RB_FPA.ipynb",
        },
    ),
]


@pytest.mark.parametrize("_types,expected_result", PARAMETERS)
def test_compare_not_fully_equal_notebooks(_types, expected_result):
    dir1 = os.path.dirname(__file__) + "/run1"
    dir2 = os.path.dirname(__file__) + "/run3"

    result = outputs.compare_notebooks(dir1, dir2, output_types_to_check=_types)
    assert set(result) == expected_result


def test_compare_not_equal_notebooks():
    dir1 = os.path.dirname(__file__) + "/run1"
    dir2 = os.path.dirname(__file__) + "/run3"

    result = outputs.compare_notebooks(dir1, dir2)
    assert set(result) == set(NOTEBOOKS)


def test_get_cell_by_tag():
    notebook_dir = os.path.dirname(__file__) + "/run1/result_AN_RQ_PNO.a6.ipynb"
    notebook = NotebookOutput(nbformat.read(notebook_dir, as_version=4))
    result_list = notebook.get_cells_by_tag("parameters")
    assert len(result_list) == 1


def test_get_cell_output():
    notebook_dir = os.path.dirname(__file__) + "/run1/result_AN_RQ_PNO.a6.ipynb"
    notebook = NotebookOutput(nbformat.read(notebook_dir, as_version=4))
    cell = notebook.get_cells_by_tag("test-tag")[0]
    stdout_output = cell.get_cell_output(CellOutputType.STDOUT)
    stderr_output = cell.get_cell_output(CellOutputType.STDERR)
    assert stdout_output[0] == "Analysis executed with lhc-sm-api version: 1.5.17"
    assert stderr_output == []


PRUNE_HTML_PARAMS = [
    (
        '{"model_id": "89066a4eb1b049ff88fef2996d058dea", "version_major": 2, "version_minor": 0}',
        '{"model_id": "", "version_major": 2, "version_minor": 0}',
    ),
    (
        "<pre>&lt;lhcsmapi.pyedsl.AssertionBuilder.AssertionBuilderSignalPlotResult at 0x7fec2db1bd90&gt;</pre>",
        "<pre>&lt;AssertionBuilderSignalPlotResult>",
    ),
    ("<pre>Elapsed: 2.350 s.", "<pre>Elapsed: s."),
    ("<pre>Analysis executed with lhc-sm-api version: 1.5.18", "<pre>lhc-sm-api version"),
    ("Analysis executed with lhc-sm-hwc notebooks version: 1.5.66", "lhc-sm-hwc version"),
    ("[NbConvertApp] Writing 375004 bytes to", "[NbConvertApp] Writing bytes to"),
    ("/tmp/lhcsmapi/analysis/dfb/DfbAnalysis.py:206: UserWarning ", "/tmp/lhcsmapi/analysis/dfb/DfbAnalysis "),
    (
        'div id="555eb0a6-dfec-4181-a339-407125800dbc"\n'
        'var gd = document.getElementById("555eb0a6-dfec-4181-a339-407125800dbc");',
        'div id=""\nvar gd = document.getElementById("");',
    ),
    (
        'div id="555eb0a6-dfec-4181-a339-407125800dbc"\n'
        'if (document.getElementById("555eb0a6-dfec-4181-a339-407125800dbc")) {\n'
        "                    Plotly.newPlot(\n"
        "                        '555eb0a6-dfec-4181-a339-407125800dbc',",
        'div id=""\n'
        'if (document.getElementById("")) {\n'
        "                    Plotly.newPlot(\n"
        "                        '',",
    ),
    ("<td>1.5.18</td>", "<td>version</td>"),
    ("<pre>CPU times: user 220 ms, sys: 137 ms, total: 357 ms", "<pre>CPU times"),
    ("<pre>CPU times: user 2 s, sys: 13.7 ms, total: 10000 ns", "<pre>CPU times"),
    ("<pre>CPU times: user 1min 34s, sys: 2.32 s, total: 1min 36s", "<pre>CPU times"),
    ("CPU times: user 1.12 s, sys: 868 µs, total: 1.12 s", "CPU times"),
    ("Wall time: 3min 57s", "Wall time"),
    ("Wall time: 3min", "Wall time"),
    ("pykernel_60236/40142755", "pykernel"),
]


@pytest.mark.parametrize("html, expected", PRUNE_HTML_PARAMS)
def test_prune_html(html, expected):
    assert outputs.prune_html(html) == expected


def test_pruned_html_reports_equal():
    report_file = "HWC_IPD_QHDA.html"
    report1 = _get_report("run1", report_file)
    report2 = _get_report("run2", report_file)
    assert outputs.prune_html(report1) == outputs.prune_html(report2)


def test_pruned_html_reports_not_equal():
    report_file = "HWC_IPD_QHDA.html"
    report1 = _get_report("run1", report_file)
    report2 = _get_report("run3", report_file)
    assert outputs.prune_html(report1) != outputs.prune_html(report2)


def _get_notebook(_dir, name):
    path = Path(os.path.dirname(__file__), _dir, name)
    return nbformat.read(path, as_version=4)


def _get_report(_dir, name):
    path = Path(os.path.dirname(__file__), _dir, name)
    return open(path, encoding="utf-8").read()
