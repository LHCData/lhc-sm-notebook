import pandas as pd
import pytest

from lhcsmnb import parameters

_ANY = None
_NB_TYPES = ["fgc", "fgc2", "qh", "acctesting"]
_NB_PARAMETERS = [
    (
        parameters.NbType.FGC,
        {
            "circuit_name": _ANY,
            "timestamp_fgc": _ANY,
            "author": _ANY,
            "is_automatic": _ANY,
            parameters.PARAMETRIZED_MARKER: _ANY,
        },
    ),
    (
        parameters.NbType.FGC2,
        {
            "circuit_type": _ANY,
            "circuit_names": _ANY,
            "timestamps_fgc": _ANY,
            "author": _ANY,
            "is_automatic": _ANY,
            parameters.PARAMETRIZED_MARKER: _ANY,
        },
    ),
    (
        parameters.NbType.QH,
        {
            "circuit_name": _ANY,
            "discharge_level": _ANY,
            "start_time": _ANY,
            "end_time": _ANY,
            "is_automatic": _ANY,
            parameters.PARAMETRIZED_MARKER: _ANY,
        },
    ),
    (
        parameters.NbType.ACCTESTING,
        {
            "hwc_test": _ANY,
            "circuit_name": _ANY,
            "campaign": _ANY,
            "t_start": _ANY,
            "t_end": _ANY,
            "is_automatic": _ANY,
            parameters.PARAMETRIZED_MARKER: _ANY,
        },
    ),
]


@pytest.mark.parametrize("notebook_type,context", _NB_PARAMETERS)
def test_are_all_parameters_injected(notebook_type, context):
    assert parameters.are_all_parameters_injected(notebook_type, context)


@pytest.mark.parametrize("notebook_type", _NB_TYPES)
def test_are_all_parameters_injected_empty(notebook_type):
    assert parameters.are_all_parameters_injected(notebook_type, {}) is False


@pytest.mark.parametrize("notebook_type,context", _NB_PARAMETERS)
def test_are_all_parameters_injected_any_missing(notebook_type, context):
    # arrange
    context_with_missing = context.copy()
    del context_with_missing["is_automatic"]

    # act
    result = parameters.are_all_parameters_injected(notebook_type, context_with_missing)

    # assert
    assert result is False


@pytest.mark.parametrize("notebook_type,context", _NB_PARAMETERS)
def test_are_all_parameters_injected_but_marker(notebook_type, context):
    # arrange
    context_with_missing = context.copy()
    del context_with_missing[parameters.PARAMETRIZED_MARKER]

    # act
    result = parameters.are_all_parameters_injected(notebook_type, context_with_missing)

    # assert
    assert result is False


def test_get_source_timestamp_df():
    # arrange
    sources = ["RD1.L2"] * 3
    timestamps = [1614851991089000000, 1614852379221000000, 1614852778066000000]
    datetime = pd.to_datetime([1614851991089000000, 1614852379221000000, 1614852778066000000])

    expected_df = pd.DataFrame({"source": sources, "timestamp": timestamps, "datetime": datetime})

    # act
    result_df = parameters.get_source_timestamp_df(sources, timestamps)

    # assert
    pd.testing.assert_frame_equal(expected_df, result_df)


def test_get_source_timestamp_df_empty():
    # arrange
    expected_columns = ["source", "timestamp", "datetime"]

    # act
    result_df = parameters.get_source_timestamp_df([], [])

    # assert
    assert result_df.empty
    assert all(expected_columns == result_df.columns)
