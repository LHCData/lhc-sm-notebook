RELEASE NOTES
=============

Version: 1.1.5
- added the module to execute the HWC notebooks on Jenkins: [SIGMON-393](https://its.cern.ch/jira/browse/SIGMON-393)
- support warnings with the severity level in the notebooks' outputs comparison: [SIGMON-466](https://its.cern.ch/jira/browse/SIGMON-466)


Version: 1.1.4
- Support for `%%time` output for notebook comparison: [SIGMON-397](https://its.cern.ch/jira/browse/SIGMON-397)
- `openjdk:8` image used for sonar

Version: 1.1.3
- Plot logic added: [SIGMON-355](https://its.cern.ch/jira/browse/SIGMON-355)
- Logic that compares notebooks' output removed empty lines: [SIGMON-303](https://its.cern.ch/jira/browse/SIGMON-303)
- RC versions of `lhcsmapi` allowed
- Bugfix: explicitly checking the lengths of lists with outputted lines

Version: 1.1.2
- Logic to prune the html reports for testing purposes: [SIGMON-201](https://its.cern.ch/jira/browse/SIGMON-201)
- Logic to retrieve cells by tags and cell output by type: [SIGMON-217](https://its.cern.ch/jira/browse/SIGMON-217)
- Sonar configured for the project

Version: 1.1.1
- Adapt notebooks to the CI: [SIGMON-141](https://its.cern.ch/jira/browse/SIGMON-141)
- IPQ query fails: [SIGMON-152](https://its.cern.ch/jira/browse/SIGMON-152)
- Analyse notebook content in the tests: [SIGMON-149](https://its.cern.ch/jira/browse/SIGMON-149)
- Fix for multiple executions: [SIGMON-202](https://its.cern.ch/jira/browse/SIGMON-202)
- Same env as on SWAN: [SIGMON-198](https://its.cern.ch/jira/browse/SIGMON-198)
- Do not crash: [SIGMON-145](https://its.cern.ch/jira/browse/SIGMON-145)
