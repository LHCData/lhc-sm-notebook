"""Contains utility methods for the parameters of the `lhc-sm-hwc` notebooks."""

import warnings
from enum import Enum
from typing import List

import pandas as pd


class NbType(Enum):
    """Denotes a type of the notebook based on its parameters"""

    FGC = 1
    FGC2 = 2
    QH = 3
    ACCTESTING = 4


# The name of the variable which needs to be injected together with the notebook's parameters
PARAMETRIZED_MARKER = "parametrized_marker"

# In order for the notebook automation (execution with papermill) to work, expected parameters gathered here need to
# be kept in sync with the notebooks and pipelines which execute them (Gitlab CI, Jenkins).
# Usually, the parameters required by the notebook can be deduced from the outputs of its 'event picker'.
# Additionally, one needs to inject PARAMETRIZED_MARKER
_PARAMETERS_TO_INJECT = {
    NbType.FGC: {"circuit_name", "timestamp_fgc", "author", "is_automatic", PARAMETRIZED_MARKER},
    NbType.FGC2: {"circuit_type", "circuit_names", "timestamps_fgc", "author", "is_automatic", PARAMETRIZED_MARKER},
    NbType.QH: {"circuit_name", "discharge_level", "start_time", "end_time", "is_automatic", PARAMETRIZED_MARKER},
    NbType.ACCTESTING: {
        "hwc_test",
        "circuit_name",
        "campaign",
        "t_start",
        "t_end",
        "is_automatic",
        PARAMETRIZED_MARKER,
    },
}


def are_all_parameters_injected(notebook_type: NbType, context: dict) -> bool:
    """Checks if all notebook parameters have been injected.

    Args:
        notebook_type: String which characterize the notebook type.
        context: Dict with the symbol table (obtained with locals() or globals()) where params are searched.
    """
    if notebook_type not in _PARAMETERS_TO_INJECT:
        warnings.warn(f'Notebook type "{notebook_type}" not known - defaults to {NbType.ACCTESTING}.')
        notebook_type = NbType.ACCTESTING

    return _PARAMETERS_TO_INJECT[notebook_type] <= context.keys()


def get_source_timestamp_df(sources: List[str], timestamps: List[str]) -> pd.DataFrame:
    """Builds input DataFrame for QH notebooks.

    Args:
        sources: List of event sources.
        timestamps: List of event timestamps.

    Returns:
        The pd.DataFrame built from the input lists.
    """
    result_df = pd.DataFrame({"source": sources, "timestamp": timestamps})
    result_df["datetime"] = pd.to_datetime(result_df["timestamp"])
    return result_df
