"""Contains common logic used in `lhc-sm-hwc` notebooks."""

from typing import Any, List

from IPython import display
from IPython.core.display import HTML
from lhcsmapi.api.analysis.output import output_types
import pandas as pd


def append_to_column_name(dataframe: pd.DataFrame, column_index: int, text_to_append: str) -> pd.DataFrame:
    """Appends a string to the name of the column indexed by column_index.

    Args:
        dataframe (pandas.DataFrame): pandas DataFrame to edit
        column_index (int): index of the column to edit
        text_to_append (str): text to append to the column's name

    Returns:
        pandas.DataFrame: DataFrame with edited column name or input DataFrame if it is empty

    Raises:
        IndexError: If column_index is outside [0, len(df.columns))
    """
    if dataframe.empty:
        return dataframe

    if not 0 <= column_index < len(dataframe.columns):
        raise IndexError("column_index is out of bounds")

    return dataframe.rename(columns={dataframe.columns[column_index]: dataframe.columns[column_index] + text_to_append})


def rename_column_name(dataframe: pd.DataFrame, column_index: int, new_name: str) -> pd.DataFrame:
    """Renames a DataFrame's column name chosen via column_index.

    Args:
        dataframe (pandas.DataFrame): pandas DataFrame to edit
        column_index (int): index of the column to rename
        new_name (str): new column name

    Returns:
        pandas.DataFrame: DataFrame with renamed column name or input DataFrame if it is empty

    Raises:
        IndexError: If column_index is outside [0, len(df.columns))
    """
    if dataframe.empty:
        return dataframe

    if not 0 <= column_index < len(dataframe.columns):
        raise IndexError("column_index is out of bounds")

    return dataframe.rename(columns={dataframe.columns[column_index]: new_name})


def get_at(dataframe: pd.DataFrame, row_label: Any, column_label: Any, default: Any = None) -> Any:
    """Gets the value at [`row_label`, `column_label`] from `df` or returns `default`."""
    if column_label in dataframe.columns and row_label in dataframe.index:
        return dataframe.at[row_label, column_label]
    return default


def display_output(output_list: List[output_types.Output]):
    """Displays the output of an Analysis.

    Args:
        output_list: A list of outputs returned by the analysis
    """

    for output in output_list:
        if isinstance(output, output_types.FigureOutput):
            display.display(output.figure)
        elif isinstance(output, output_types.HTMLOutput):
            display.display(HTML(output.html))
        elif isinstance(output, output_types.TextOutput):
            display.display(output.text)
